<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/29/2019
 * Time: 5:47 AM
 */

namespace App\Tests;

use App\Entity\OfferCollectionInterface;
use PHPUnit\Framework\TestCase;
use App\Service\DataReader\JsonReader;

class TestJsonToObjectConversion extends TestCase
{
    public function testJsonConversion()
    {
        $dataFormat = 'json';
        $data = '[
                  {
                    "title": "Becoming",
                    "price": "14.99",
                    "quantity": 10,
                    "dateAdded": "2017-05-02 12:00:31",
                    "vendorId": 1
                  },
                  {
                    "title": "Where the Crawdads Sing",
                    "price": "10.87",
                    "quantity": 0,
                    "dateAdded": "2019-01-02 10:00:31",
                    "vendorId": 1
                  }]';

        $jsonReader = new JsonReader($dataFormat);
        $result = $jsonReader->read($data);

        $this->assertInstanceOf(OfferCollectionInterface::class, $result);
        $this->assertCount(2, $result);
    }

}