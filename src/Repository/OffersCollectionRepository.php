<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/29/2019
 * Time: 2:56 AM
 */

namespace App\Repository;


use App\Entity\OfferCollection;
use App\Entity\OfferCollectionInterface;
use App\Entity\OfferInterface;


/**
 * Class OffersCollectionRepository
 * @package App\Repository
 */
class OffersCollectionRepository
{
    private $offers;
    private $iterator;

    public function __construct(OfferCollectionInterface $offers)
    {
        $this->offers = $offers;
        $this->iterator = $this->offers->getIterator();
    }


    /**
     * @param int $vendorId
     * @return int
     */
    public function getCountByVendor(int $vendorId): int
    {
        $vendorOfferCount = 0;

        foreach ($this->iterator as $offer) {
            if ($this->hasStock($offer) && $offer->getVendorId() === $vendorId) {
                $vendorOfferCount += $offer->getQuantity();
            }
        }

        return $vendorOfferCount;
    }


    /**
     * @param float $startPrice
     * @param float $endPrice
     * @return OfferCollectionInterface
     */
    public function getByPriceRange(float $startPrice, float $endPrice): OfferCollectionInterface
    {
        $filtered = new OfferCollection();

        foreach ($this->iterator as $offer) {
            if ($this->hasStock($offer)
                && $offer->getPrice() >= $startPrice
                && $offer->getPrice() <= $endPrice
            ) {
                $filtered->add($offer);
            }
        }

        return $filtered;

    }


    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return OfferCollectionInterface
     */
    public function getByDatePeriod(\DateTime $startDate, \DateTime $endDate): OfferCollectionInterface
    {
        $filtered = new OfferCollection();

        foreach ($this->iterator as $offer) {
            if ($this->hasStock($offer)
                && $startDate->getTimestamp() <= $offer->getDateAdded()->getTimestamp()
                && $endDate->getTimestamp() >= $offer->getDateAdded()->getTimestamp()
            ) {
                $filtered->add($offer);
            }
        }

        return $filtered;
    }


    /**
     * @param OfferInterface $offer
     * @return bool
     */
    private function hasStock(OfferInterface $offer): bool
    {
        if ($offer->getQuantity() > 0) {
            return true;
        }

        return false;
    }
}