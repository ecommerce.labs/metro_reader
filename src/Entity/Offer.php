<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/28/2019
 * Time: 12:30 PM
 */

namespace App\Entity;


class Offer implements OfferInterface
{

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $dateAdded;

    /**
     * @var float
     */
    private $price;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var int
     */
    private $vendorId;


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }


    /**
     * @param string $title
     * @return Offer
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdded(): \DateTime
    {
        return $this->dateAdded;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }


    /**
     * @param float $price
     * @return Offer
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }


    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }


    /**
     * @param $quantity
     * @return Offer
     */
    public function setQuantity($quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }


    /**
     * @return int
     */
    public function getVendorId(): int
    {
        return $this->vendorId;
    }


    /**
     * @param string $dateAdded
     * @return Offer
     * @throws \Exception
     */
    public function setDateAdded(string $dateAdded): self
    {
        $this->dateAdded = new \DateTime($dateAdded);

        return $this;
    }


    /**
     * @param int $vendorId
     * @return Offer
     */
    public function setVendorId(int $vendorId): self
    {
        $this->vendorId = $vendorId;

        return $this;
    }


}