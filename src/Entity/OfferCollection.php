<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/28/2019
 * Time: 12:30 PM
 */

namespace App\Entity;


use ArrayIterator;

class OfferCollection implements OfferCollectionInterface
{
    /**
     * @var array
     */
    private $offers = [];


    /**
     * @param OfferInterface $offer
     */
    public function add(OfferInterface $offer): void
    {
        $this->offers[] = $offer;
    }

    /**
     * @param int $index
     * @return OfferInterface
     */
    public function get(int $index): OfferInterface
    {
        return $this->offers[$index];
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->offers);
    }

}