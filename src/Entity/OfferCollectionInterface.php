<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/28/2019
 * Time: 12:29 PM
 */

namespace App\Entity;

use ArrayIterator;

interface OfferCollectionInterface
{
    /**
     * @param OfferInterface $offer
     * @return void
     */
    public function add(OfferInterface $offer): void;

    /**
     * @param int $index
     * @return OfferInterface
     */
    public function get(int $index): OfferInterface;

    /**
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator;
}