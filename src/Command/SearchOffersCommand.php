<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/28/2019
 * Time: 6:12 PM
 */

namespace App\Command;


use App\Repository\OffersCollectionRepository;
use App\Service\Console\OfferFinderConsoleService;
use App\Service\HttpResponseToObjectMapper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SearchOffersCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:offers:search';

    /**
     * @var HttpResponseToObjectMapper
     */
    private $httpResponseToObjectMapper;

    /**
     * @var OfferFinderConsoleService
     */
    private $offerFinder;


    /**
     * SearchOffersCommand constructor.
     * @param null $name
     * @param HttpResponseToObjectMapper $httpResponseToObjectMapper
     * @param OfferFinderConsoleService $offerFinder
     */
    public function __construct(
        $name = null,
        HttpResponseToObjectMapper $httpResponseToObjectMapper,
        OfferFinderConsoleService $offerFinder)
    {
        parent::__construct($name);

        $this->offerFinder = $offerFinder;
        $this->httpResponseToObjectMapper = $httpResponseToObjectMapper;
    }

    protected function configure(): void
    {
        $this->setDescription("Searching offers in JSON Response");
        $this->setDefinition(
            new InputDefinition(
                [
                    new InputOption('vendor', 'vid', InputOption::VALUE_OPTIONAL),
                    new InputOption('start_price', 'sp', InputOption::VALUE_OPTIONAL),
                    new InputOption('end_price', 'ep', InputOption::VALUE_OPTIONAL),
                    new InputOption('start_date', 'sd', InputOption::VALUE_OPTIONAL),
                    new InputOption('end_date', 'ed', InputOption::VALUE_OPTIONAL)
                ]
            )
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Call our route and get result as a collection of objects
        $offers = $this->httpResponseToObjectMapper->getObjects();

        print_r($offers);

        //get filtered objects
        $filteredOffers = $this->offerFinder->getConsoleInput($input)->searchCollection($offers);

        //print_r($filteredOffers);

        //$output->write($offersRepository->getByDatePeriod(new \DateTime('2015-01-01 00:00:00'), new \DateTime('2019-04-01 00:00:00')));



        $offersRepository = new OffersCollectionRepository($offers);

        print_r($offersRepository->getByDatePeriod(new \DateTime('2015-01-01 00:00:00'), new \DateTime('2019-04-01 00:00:00')));
    }

}