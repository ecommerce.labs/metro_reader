<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/28/2019
 * Time: 5:59 PM
 */

namespace App\Controller;

use App\Service\DataSource\DataSourceManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class OfferController
 * @package App\Controller
 */
class OfferController extends AbstractController
{

    /**
     * @var string
     */
    private $fileFormat;


    /**
     * @var DataSourceManager
     */
    private $dataSourceManager;


    /**
     * OfferController constructor.
     * @param DataSourceManager $dataSourceManager
     */
    public function __construct(DataSourceManager $dataSourceManager)
    {
        $this->dataSourceManager = $dataSourceManager;
    }


    /**
     * @Route("offer/get", name="get_offers", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function getOffers(Request $request): Response
    {
        $fileFormat = $request->request->get('fileFormat');
        $content = $this->dataSourceManager->getContent($fileFormat);

        return new Response(
            $content,
            Response::HTTP_OK,
            ['content-type' => 'text/' . $this->fileFormat]
        );
    }
}