<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/29/2019
 * Time: 3:13 AM
 */

namespace App\Service;


use App\Entity\OfferCollectionInterface;
use App\Service\DataReader\ReaderFactory;
use Curl\Curl;

class HttpResponseToObjectMapper
{
    /**
     * @var string
     */
    private $endPointUrl;

    /**
     * @var string
     */
    private $dataFormat;

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var ReaderFactory
     */
    private $readerFactory;


    /**
     * HttpResponseToObjectMapper constructor.
     * @param string $endPointUrl
     * @param string $dataFormat
     * @param ReaderFactory $readerFactory
     * @throws \ErrorException
     */
    public function __construct(string $endPointUrl, string $dataFormat, ReaderFactory $readerFactory)
    {
        $this->curl = new Curl();
        $this->endPointUrl = $endPointUrl;
        $this->dataFormat = $dataFormat;
        $this->readerFactory = $readerFactory;
    }


    /**
     * @return OfferCollectionInterface
     * @throws \Exception
     */
    public function getObjects(): OfferCollectionInterface
    {
        $reader = $this->readerFactory->getReader($this->dataFormat);

        return $reader->read($this->getResponse());
    }


    /**
     * @return string
     */
    private function getResponse(): string
    {
        $this->curl->post($this->endPointUrl, [
            "fileFormat" => $this->dataFormat
        ]);

        return $this->curl->response;
    }


    /**
     * Destructor - close CURL connection
     */
    public function __destruct()
    {
        $this->curl->close();
    }
}