<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/29/2019
 * Time: 9:27 AM
 */

namespace App\Service\Console;


use App\Entity\OfferCollectionInterface;
use App\Repository\OffersCollectionRepository;
use phpDocumentor\Reflection\Types\Mixed_;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Class OfferFinderConsoleService
 * @package App\Service\Console
 */
class OfferFinderConsoleService
{
    /**
     * @var InputInterface
     */
    private $consoleInput;


    public function getConsoleInput(InputInterface $consoleInput)
    {
        $this->consoleInput = $consoleInput;

        return $this;
    }


    public function searchCollection(OfferCollectionInterface $offerCollection)
    {
        if (!$this->consoleInput) {
            throw new \Exception("ObjectFinder hasn't been initialized");
        }

        $offerRepository = new OffersCollectionRepository($offerCollection);

        $vendorId = $this->consoleInput->getOption('vendor');

        if (isset($vendorId)) {
            //run search by Vendor
            print_r('test');

            return $offerRepository->getCountByVendor($vendorId);
        }




    }


}