<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/28/2019
 * Time: 1:10 PM
 */

namespace App\Service\DataReader;


use App\Entity\OfferCollection;
use App\Entity\OfferCollectionInterface;
use App\Entity\Offer;
use App\Entity\OfferInterface;


/**
 * Class JsonReader
 * @package App\Service\DataReader
 */
class JsonReader extends AbstractReader implements ReaderInterface
{

    /**
     * JsonReader constructor.
     * @throws \ReflectionException
     */
    public function __construct()
    {
        parent::__construct(self::FORMAT_TYPE_JSON);
    }


    /**
     * @param string $input
     * @return OfferCollectionInterface
     */
    public function read(string $input): OfferCollectionInterface
    {
        /**
         * @var $offers Offer[]
         */
        $offers = $this->serializer->deserialize($input, Offer::class . "[]", $this->readerFormat);

        if (count($offers) > 0) {
            foreach ($offers as $offer) {
                if ($offer instanceof OfferInterface) {
                    $this->offerCollection->add($offer);
                }
            }
        }

        return $this->offerCollection;
    }
}