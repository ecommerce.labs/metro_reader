<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/29/2019
 * Time: 1:06 AM
 */

namespace App\Service\DataReader;


use App\Entity\OfferCollection;
use App\Entity\Offer;
use App\Entity\OfferCollectionInterface;
use App\Entity\OfferInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;


/**
 * Class AbstractReader
 * @package App\Service\DataReader
 */
abstract class AbstractReader
{
    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var OfferCollection
     */
    protected $offerCollection;

    /**
     * @var string
     */
    protected $readerFormat;

    /**
     * Source format
     */
    const FORMAT_TYPE_XML = 'xml';
    const FORMAT_TYPE_JSON = 'json';


    /**
     * AbstractReader constructor.
     * @param $readerFormat
     * @throws \ReflectionException
     */
    public function __construct($readerFormat)
    {
        $this->readerFormat = $readerFormat;

        $this->offerCollection = new OfferCollection();

        $this->serializer = new Serializer(
            [new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [$this->getEncoder()]
        );
    }


    /**
     * @return EncoderInterface
     * @throws \ReflectionException
     */
    private function getEncoder(): EncoderInterface
    {
        $encoderClassNamespace = (new \ReflectionClass(EncoderInterface::class))->getNamespaceName();

        $encoder = $encoderClassNamespace . "\\" . ucfirst(strtolower($this->readerFormat)) . "Encoder";

        return new $encoder();
    }
}