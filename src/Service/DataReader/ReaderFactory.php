<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/29/2019
 * Time: 12:57 AM
 */

namespace App\Service\DataReader;


/**
 * Class ReaderFactory
 * @package App\Service\DataReader
 */
class ReaderFactory
{

    /**
     * @param $fileFormat
     * @return ReaderInterface
     * @throws \Exception
     */
    public function getReader($fileFormat): ReaderInterface
    {
        $reader = __NAMESPACE__ . "\\" . ucfirst(strtolower($fileFormat)) . "Reader";

        if (!class_exists($reader)) {
            throw new \Exception("Can't find " . $reader . " reader");
        }

        return new $reader();
    }
}