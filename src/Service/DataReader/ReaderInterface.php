<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/28/2019
 * Time: 1:09 PM
 */

namespace App\Service\DataReader;


use App\Entity\OfferCollectionInterface;

interface ReaderInterface
{
    public function read(string $input): OfferCollectionInterface;
}