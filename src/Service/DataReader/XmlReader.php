<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/29/2019
 * Time: 12:48 AM
 */

namespace App\Service\DataReader;


use App\Entity\Offer;
use App\Entity\OfferCollectionInterface;


/**
 * Class XmlReader
 * @package App\Service\DataReader
 */
class XmlReader extends AbstractReader implements ReaderInterface
{
    /**
     * XmlReader constructor.
     * @throws \ReflectionException
     */
    public function __construct()
    {
        parent::__construct(self::FORMAT_TYPE_XML);
    }


    /**
     * @param string $input
     * @return OfferCollectionInterface
     * @throws \Exception
     */
    public function read(string $input): OfferCollectionInterface
    {
        $offers = simplexml_load_string($input);

        if (count($offers) > 0) {
            foreach ($offers as $offer) {
                $collectionItem = new Offer();
                $collectionItem->setTitle((string)$offer->title)
                    ->setPrice((float)$offer->price)
                    ->setQuantity((int)$offer->quantity)
                    ->setDateAdded((string)$offer->dateAdded)
                    ->setVendorId((int)$offer->vendorId);

                $this->offerCollection->add($collectionItem);
            }
        }

        return $this->offerCollection;
    }
}