<?php
/**
 * Created by PhpStorm.
 * User: net
 * Date: 3/28/2019
 * Time: 6:31 PM
 */

namespace App\Service\DataSource;


use Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;


/**
 * Class FileLoader
 * @package App\Service
 */
class DataSourceManager
{
    /**
     * @var string
     */
    private $resourcesDir;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var Finder
     */
    private $finder;


    /**
     * DataSourceManager constructor.
     * @param string $resourcesDir
     * @param Filesystem $fileSystem
     */
    public function __construct(string $resourcesDir, Filesystem $fileSystem)
    {
        $this->resourcesDir = $resourcesDir;
        $this->fileSystem = $fileSystem;
        $this->finder = new Finder();
    }


    /**
     * @param string $fileFormat
     * @return string
     * @throws Exception
     */
    public function getContent(string $fileFormat): string
    {
        $fileName = $this->getFilePath($fileFormat);

        if ($fileName === null) {
            throw new Exception("Can't find Offer file with " . $fileFormat . " extension");
        }

        return file_get_contents($fileName);
    }


    /**
     * @param string $fileFormat
     * @return string|null
     */
    private function getFilePath(string $fileFormat): ?string
    {
        if ($this->fileSystem->exists($this->resourcesDir)) {
            $this->finder->files()->in($this->resourcesDir);
            foreach ($this->finder as $file) {
                $filePath = $file->getPathname();
                $fileExtension = $this->getFileExtension($filePath);

                if ($fileExtension === $fileFormat) {
                    return $filePath;
                }
            }
        }

        return null;
    }


    /**
     * @param string $fileName
     * @return string
     */
    private function getFileExtension(string $fileName): string
    {
        $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);

        return $fileExtension;
    }
}